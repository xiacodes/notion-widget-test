const nameInput = document.querySelector("#nameInput");
const favColourInput = document.querySelector("#favColourInput");
const renderBtn = document.querySelector("button");
const linkTextBox = document.querySelector("#linkToRender");
const renderBox = document.querySelector(".render-box");
const container = renderBox.querySelector(".container");
const h2 = container.querySelector("h2");
let defaultLink = "http://127.0.0.1:5500/render.html"


//  ------------------------------------------ //

renderBtn.addEventListener("click", function () {
    let name = nameInput.value;
    let colour = favColourInput.value;

    //TODO: check if the name and colour has any spaces

    let query = "?colour=" + colour + "&name=" + name;

    let newLink = defaultLink + query;
    openRender(newLink);

    linkTextBox.value = newLink;

});

function openRender(link) {
    let a = document.createElement("a");
    a.href = link;
    a.click();
}

setInterval(() => {
    let name = nameInput.value;
    let colour = favColourInput.value;
    
    container.style.backgroundColor = colour;
    h2.textContent = name;
}, 100);